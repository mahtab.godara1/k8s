[
  {
    "subtext": "",
    "text": "Identify the certificate file used for the `kube-api server`",
    "type": "multiple_choice",
    "options": [
      "/etc/kubernetes/pki/apiserver.crt",
      "/etc/apiserver.crt",
      "/etc/apiserver.key",
      "/tmp/kube-apiserver.crt",
      "/etc/kubernetes/pki/kube-apiserver.crt"
    ],
    "answer": "/etc/kubernetes/pki/apiserver.crt"
  },
  {
    "text": "Identify the Certificate file used to authenticate `kube-apiserver` as a client to `ETCD` Server",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Run the command 'cat /etc/kubernetes/manifests/kube-apiserver.yaml' and look for etcd configurations",
    "options": [
      "/etc/kubernetes/pki/apiserver-etcd-client.crt",
      "/etc/kubernetes/pki/apiserver-kubelet-client.crt",
      "/etc/kubernetes/pki/apiserver-etcd-client.key",
      "/etc/kubernetes/pki/apiserver.crt",
      "/etc/kubernetes/pki/apiserver-etcd.crt"
    ],
    "answer": "/etc/kubernetes/pki/apiserver-etcd-client.crt"
  },
  {
    "text": "Identify the key used to authenticate `kubeapi-server` to the `kubelet` server",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Look for kubelet-client-key option in the file `/etc/kubernetes/manifests/kube-apiserver.yaml`",
    "options": [
      "/etc/kubernetes/pki/front-proxy-client.key",
      "/etc/kubernetes/pki/apiserver-kubelet-client.key",
      "/etc/kubernetes/pki/apiserver-kubelet-client.crt",
      "/etc/kubernetes/pki/apiserver.key",
      "/etc/kubernetes/pki/apiserver-etcd-client.key"
    ],
    "answer": "/etc/kubernetes/pki/apiserver-kubelet-client.key"
  },
  {
    "text": "Identify the ETCD Server Certificate used to host ETCD server",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Look for cert file option in the file `/etc/kubernetes/manifests/etcd.yaml`",
    "options": [
      "/etc/kubernetes/pki/apiserver-etcd-client.crt",
      "/etc/kubernetes/pki/apiserver.crt",
      "/etc/kubernetes/pki/etcd/server.crt",
      "/etc/kubernetes/pki/etcd/ca.crt"
    ],
    "answer": "/etc/kubernetes/pki/etcd/server.crt"
  },
  {
    "text": "Identify the ETCD Server CA Root Certificate used to serve ETCD Server",
    "subtext": "ETCD can have its own CA. So this may be a different CA certificate than the one used by kube-api server.",
    "type": "multiple_choice",
    "hint": "Look for CA Certificate in file `/etc/kubernetes/manifests/etcd.yaml`",
    "options": [
      "/etc/kubernetes/pki/etcd/server.crt",
      "/etc/kubernetes/pki/ca.crt",
      "/etc/kubernetes/pki/etcd/ca.crt",
      "/etc/kubernetes/pki/apiserver-kubelet-client.crt"
    ],
    "answer": "/etc/kubernetes/pki/etcd/ca.crt"
  },
  {
    "text": "What is the Common Name (CN) configured on the Kube API Server Certificate?",
    "subtext": "**OpenSSL Syntax:** `openssl x509 -in file-path.crt -text -noout`",
    "type": "multiple_choice",
    "hint": "Run the command `openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text`",
    "options": [
      "kubernetes",
      "api-server",
      "kube-api-server",
      "kube-apiserver",
      "kubeapi-server"
    ],
    "answer": "kube-apiserver"
  },
  {
    "text": "What is the name of the CA who issued the Kube API Server Certificate?",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Run the command `openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text` and look for issuer",
    "options": [
      "kubernetes-ca",
      "ca",
      "kubernetes",
      "kube-apiserver"
    ],
    "answer": "kubernetes"
  },
  {
    "text": "Which of the below alternate names is not configured on the Kube API Server Certificate?",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Run the command `openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text` and look at Alternative Names",
    "options": [
      "kubernetes",
      "kubernetes.default.svc",
      "master",
      "kube-master"
    ],
    "answer": "kube-master"
  },
  {
    "text": "What is the Common Name (CN) configured on the ETCD Server certificate?",
    "subtext": "",
    "type": "multiple_choice",
    "hint": "Run the command `openssl x509 -in /etc/kubernetes/pki/etcd/server.crt -text` and look for Subject CN.",
    "options": [
      "etcd-server",
      "etcd",
      "master",
      "kubernetes"
    ],
    "answer": "master"
  },
  {
    "text": "How long, from the issued date, is the Kube-API Server Certificate valid for?",
    "subtext": "File: `/etc/kubernetes/pki/apiserver.crt`",
    "type": "multiple_choice",
    "hint": "Run the command `openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text` and check on the Expiry date.",
    "options": [
      "1 Year",
      "2 Years",
      "10 years",
      "6 months"
    ],
    "answer": "1 Year"
  },
  {
    "text": "How long, from the issued date, is the Root CA Certificate valid for?",
    "subtext": "File: `/etc/kubernetes/pki/ca.crt`",
    "type": "multiple_choice",
    "hint": "Run the command 'openssl x509 -in /etc/kubernetes/pki/ca.crt -text' and look for validity",
    "options": [
      "6 Months",
      "1 Year",
      "2 Years",
      "10 years"
    ],
    "answer": "10 years"
  },
  {
    "before": [
      {
        "type": "command",
        "command": [
          "cp",
          "/etc/kubernetes/manifests/etcd.yaml",
          "/var/answers"
        ]
      },
      {
        "type": "command",
        "command": [
          "sed",
          "-i",
          "s/--cert-file=\\/etc\\/kubernetes\\/pki\\/etcd\\/server.crt/--cert-file=\\/etc\\/kubernetes\\/pki\\/etcd\\/server-certificate.crt/",
          "/etc/kubernetes/manifests/etcd.yaml"
        ]
      },
      {
        "type": "command",
        "action": "create",
        "config": {
          "apiVersion": "v1",
          "kind": "Pod",
          "metadata": {
            "name": "simple-webapp-1",
            "labels": {
              "name": "simple-webapp"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "simple-webapp",
                "image": "kodekloud/webapp-delayed-start",
                "ports": [
                  {
                    "containerPort": 8080
                  }
                ]
              }
            ]
          }
        },
        "command": "sleep 10",
        "shell": true
      }
    ],
    "text": "Kubectl suddenly stops responding to your commands. Check it out! Someone recently modified the `/etc/kubernetes/manifests/etcd.yaml` file",
    "subtext": "You are asked to investigate and fix the issue. Once you fix the issue wait for sometime for kubectl to respond. Check the logs of the ETCD container.",
    "type": "config_test",
    "hint": "Inspect the --cert-file option in the manifests file.",
    "tests": [
      {
        "type": "kubernetes",
        "object": "Pod",
        "name": "kube-apiserver-master",
        "namespace": "kube-system",
        "spec": "Fix the kube-api server",
        "err_message": "kube-api server not running",
        "jmespath": "items[].metadata.name",
        "state": "present"
      }
    ],
    "staging_message": "Going through a change window! Hold on!"
  },
  {
    "before": [
      {
        "type": "command",
        "command": [
          "cp",
          "/etc/kubernetes/manifests/kube-apiserver.yaml",
          "/var/answers"
        ]
      },
      {
        "type": "command",
        "command": [
          "sed",
          "-i",
          "s/--etcd-cafile=\\/etc\\/kubernetes\\/pki\\/etcd\\/ca.crt/--etcd-cafile=\\/etc\\/kubernetes\\/pki\\/ca.crt/",
          "/etc/kubernetes/manifests/kube-apiserver.yaml"
        ]
      },
      {
        "type": "command",
        "action": "create",
        "config": {
          "apiVersion": "v1",
          "kind": "Pod",
          "metadata": {
            "name": "simple-webapp-1",
            "labels": {
              "name": "simple-webapp"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "simple-webapp",
                "image": "kodekloud/webapp-delayed-start",
                "ports": [
                  {
                    "containerPort": 8080
                  }
                ]
              }
            ]
          }
        },
        "command": "sleep 10",
        "shell": true
      }
    ],
    "text": "The kube-api server stopped again! Check it out. Inspect the kube-api server logs and identify the root cause and fix the issue.",
    "staging_message": "After a few hours!",
    "subtext": "Run `docker ps -a` command to identify the kube-api server container. Run `docker logs container-id` command to view the logs.",
    "type": "config_test",
    "hint": "ETCD has its own CA. The right CA must be used for the ETCD-CA file in /etc/kubernetes/manifests/kube-apiserver.yaml. View answer at /var/answers/kube-apiserver.yaml",
    "tests": [
      {
        "type": "kubernetes",
        "object": "Pod",
        "name": "kube-apiserver-master",
        "namespace": "kube-system",
        "spec": "Fix the kube-api server",
        "err_message": "kube-api server not running",
        "jmespath": "items[].metadata.name",
        "state": "present"
      }
    ]
  }
  
]
