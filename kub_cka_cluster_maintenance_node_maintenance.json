[
  {
    "answer_from": {
      "jmespath": "length(items)",
      "object": "Node",
      "type": "kubernetes"
    },
    "before": [],
    "hint": "",
    "options": [
      1,
      2,
      3
    ],
    "subtext": "Including the master and workers",
    "text": "Let us explore the environment first. How many nodes do you see in the cluster?",
    "type": "multiple_choice"
  },
  {
    "answer_from": {
      "jmespath": "length(items)",
      "namespace": "default",
      "object": "Deployment",
      "type": "kubernetes"
    },
    "before": [
      {
        "action": "create",
        "config": {
          "apiVersion": "apps/v1",
          "kind": "Deployment",
          "metadata": {
            "labels": {
              "app": "red"
            },
            "name": "red"
          },
          "spec": {
            "replicas": 2,
            "selector": {
              "matchLabels": {
                "app": "red"
              }
            },
            "template": {
              "metadata": {
                "labels": {
                  "app": "red"
                }
              },
              "spec": {
                "containers": [
                  {
                    "image": "nginx:alpine",
                    "name": "nginx",
                    "ports": [
                      {
                        "containerPort": 80
                      }
                    ]
                  }
                ]
              }
            }
          }
        },
        "type": "kubernetes"
      },
      {
        "action": "create",
        "config": {
          "apiVersion": "apps/v1",
          "kind": "Deployment",
          "metadata": {
            "labels": {
              "app": "blue"
            },
            "name": "blue"
          },
          "spec": {
            "ports": [
              {
                "nodePort": 30082,
                "port": 80
              }
            ],
            "replicas": 3,
            "selector": {
              "matchLabels": {
                "app": "blue"
              }
            },
            "template": {
              "metadata": {
                "labels": {
                  "app": "blue"
                }
              },
              "spec": {
                "containers": [
                  {
                    "image": "nginx:alpine",
                    "name": "nginx",
                    "ports": [
                      {
                        "containerPort": 80
                      }
                    ]
                  }
                ]
              }
            }
          }
        },
        "type": "kubernetes"
      }
    ],
    "hint": "",
    "options": [
      1,
      2,
      3,
      5
    ],
    "subtext": "Check the number of deployments",
    "text": "How many applications do you see hosted on the cluster?",
    "type": "multiple_choice"
  },
  {
    "answer_from": {
      "command": "kubectl get pods -o wide --no-headers | awk '{print $7}' | sort -u | tr '\\n' ',' | sed 's/\\(.*\\),/\\1/'",
      "shell": true,
      "type": "command"
    },
    "hint": "Run the command 'kubectl get pods -o wide' and get the list of nodes the pods are placed on",
    "options": [
      "master,node01",
      "node01,node02",
      "master,node03"
    ],
    "text": "On which nodes are the applications hosted on?",
    "type": "multiple_choice"
  },
  {
    "hint": "Run the command `kubectl drain node01 --ignore-daemonsets`",
    "staging_message": "Setting things up",
    "subtext": "",
    "tests": [
      {
        "custom_api": "/api/v1/nodes/node01",
        "jmespath": "spec.unschedulable",
        "name": "true",
        "namespace": "default",
        "object": "custom",
        "spec": "Node node01 Unschedulable",
        "state": "equal",
        "type": "command",
        "command": "kubectl get nodes | grep node01 | grep \"SchedulingDisabled\"",
        "shell": true
      },
      {
        "custom_api": "/api/v1/nodes/node01",
        "jmespath": "spec.unschedulable",
        "name": "true",
        "namespace": "default",
        "object": "custom",
        "spec": "Pods evicted from node01",
        "state": "equal",
        "type": "command",
        "command": "bash /tmp/ts.sh",
        "shell": true
      }
    ],
    "text": "We need to take `node01` out for maintenance. Empty the node of all applications and mark it unschedulable.",
    "type": "config_test"
  },
  {
    "answer_from": {
      "command": "kubectl get pods -o wide --no-headers | awk '{print $7}' | sort -u | tr '\\n' ',' | sed 's/\\(.*\\),/\\1/'",
      "shell": true,
      "type": "command"
    },
    "hint": "Run the command 'kubectl get pods -o wide' and get the list of nodes the pods are placed on",
    "options": [
      "master,node01",
      "master,node02",
      "node01,node02"
    ],
    "staging_message": "Setting things up",
    "subtext": "",
    "text": "What nodes are the apps on now?",
    "type": "multiple_choice"
  },
  {
    "before": [
      {
        "action": "create",
        "command": "sleep 5",
        "config": {
          "apiVersion": "v1",
          "kind": "Pod",
          "metadata": {
            "labels": {
              "name": "simple-webapp"
            },
            "name": "simple-webapp-1"
          },
          "spec": {
            "containers": [
              {
                "image": "kodekloud/webapp-delayed-start",
                "name": "simple-webapp",
                "ports": [
                  {
                    "containerPort": 8080
                  }
                ]
              }
            ]
          }
        },
        "shell": true,
        "type": "command"
      }
    ],
    "hint": "Run the command `kubectl uncordon node01`",
    "staging_message": "Applying patches on Node01. Hold on!",
    "subtext": "",
    "tests": [
      {
        "command": "kubectl get nodes | grep node01 | grep -v SchedulingDisabled",
        "object": "custom",
        "shell": true,
        "spec": "Node01 is Schedulable",
        "state": "equal",
        "type": "command"
      }
    ],
    "text": "The maintenance tasks have been completed. Configure the node to be schedulable again.",
    "type": "config_test"
  },
  {
    "answer": null,
    "answer_from": {
      "command": "kubectl get pods -o wide | grep node01 | wc -l",
      "shell": true,
      "type": "command"
    },
    "hint": "Run the command `kubectl get pods -o wide`",
    "options": [
      "0",
      "1",
      "2",
      "3"
    ],
    "staging_message": "Setting things up",
    "subtext": "",
    "text": "How many pods are scheduled on `node01` now?",
    "type": "multiple_choice"
  },
  {
    "answer": "Only when new pods are created they will be scheduled",
    "hint": "",
    "options": [
      "node01 is cordoned",
      "node01 is faulty",
      "node01 did not upgrade successfully",
      "Only when new pods are created they will be scheduled"
    ],
    "staging_message": "Setting things up",
    "subtext": "",
    "text": "Why are there no pods on `node01`?",
    "type": "multiple_choice"
  },
  {
    "answer": "master node has taints set on it",
    "hint": "Use the command `kubectl describe node master`",
    "options": [
      "master node is cordoned",
      "master node is faulty",
      "master node has taints set on it",
      "you can never have pods on master nodes"
    ],
    "staging_message": "Setting things up",
    "subtext": "Check the master node details",
    "text": "Why are there no pods placed on the `master` node?",
    "type": "multiple_choice"
  },
  {
    "answer": "No, you must force it",
    "before": [
      {
        "action": "create",
        "config": {
          "apiVersion": "v1",
          "kind": "Pod",
          "metadata": {
            "name": "hr-app"
          },
          "spec": {
            "containers": [
              {
                "image": "nginx:alpine",
                "name": "hr-app",
                "ports": [
                  {
                    "containerPort": 80
                  }
                ]
              }
            ],
            "nodeName": "node02"
          }
        },
        "type": "kubernetes"
      }
    ],
    "hint": "Run the command `kubectl drain node02 --ignore-daemonsets`",
    "options": [
      "Yes",
      "No, you must force it"
    ],
    "staging_message": "Setting things up",
    "subtext": "Can you drain node02 using the same command as node01? Try it.",
    "text": "It is now time to take down `node02` for maintenance. Before you remove all workload from `node02` answer the following question.",
    "type": "multiple_choice"
  },
  {
    "answer": "node02 has a pod not part of a replicaset",
    "hint": "Check the applications hosted on the node02.",
    "options": [
      "Node02 is faulty",
      "node02 has taints on it",
      "node02 has a pod not part of a replicaset",
      "Only one node can be taken down in a day"
    ],
    "staging_message": "Setting things up",
    "subtext": "",
    "text": "Why do you need to `force` the drain?",
    "type": "multiple_choice"
  },
  {
    "answer": "hr-app",
    "hint": "Check the list of pods",
    "options": [
      "red",
      "blue",
      "hr-app",
      "redis"
    ],
    "staging_message": "Setting things up",
    "subtext": "",
    "text": "What is the name of the POD not part of a replicaset hosted on `node02`?",
    "type": "multiple_choice"
  },
  {
    "answer": "hr-app will be lost forever",
    "hint": "",
    "options": [
      "hr-app will be recreated on other nodes",
      "hr-app will be lost forever",
      "hr-app will be re-created on master",
      "hr-app will continue to run as a Docker container"
    ],
    "staging_message": "Setting things up",
    "subtext": "Try it if you wish",
    "text": "What would happen to `hr-app` if `node02` is drained forcefully?",
    "type": "multiple_choice"
  },
  {
    "hint": "Run the command `kubectl drain node02 --ignore-daemonsets --force`",
    "staging_message": "Setting things up",
    "subtext": "",
    "tests": [
      {
        "custom_api": "/api/v1/nodes/node02",
        "jmespath": "spec.unschedulable",
        "name": false,
        "namespace": "default",
        "object": "custom",
        "spec": "Node02 Drained",
        "state": "equal",
        "type": "command",
        "command": "kubectl get nodes | grep node02 | grep \"SchedulingDisabled\"",
        "shell": true
      }
    ],
    "text": "Drain `node02` and mark it `unschedulable`",
    "type": "config_test"
  },
  {
    "hint": "Run the command `kubectl cordon node03`",
    "staging_message": "Setting things up",
    "subtext": "",
    "tests": [
      {
        "command": "kubectl get pods -o wide | grep node03 && kubectl get nodes | grep node03 | grep SchedulingDisabled",
        "jmespath": "items[].metadata.name",
        "name": "ingress-space",
        "namespace": "default",
        "object": "Namespace",
        "shell": true,
        "spec": "Node03 Unschedulable",
        "state": "present",
        "type": "command"
      },
      {
        "command": "kubectl get pods -o wide | grep node03",
        "jmespath": "items[].metadata.name",
        "name": "ingress-space",
        "namespace": "default",
        "object": "Namespace",
        "shell": true,
        "spec": "Node03 has apps",
        "state": "present",
        "type": "command"
      }
    ],
    "text": "Node03 has our critical applications. We do not want to schedule any more apps on node03. Mark `node03` as `unschedulable` but do not remove any apps currently running on it .",
    "type": "config_test"
  }
]